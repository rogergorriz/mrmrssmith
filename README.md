# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

Clone this repository in your preferred place.

Run `php -S localhost:8000 -t public`

Now the web application will be listening in localhost:8000.

If you want to run the tests you have to run `bin/phpunit`

### General notes ###

I made it as much scalable as possible following the Hexagonal Architecture principles. It may seem an over-engineered solution because of the simplicity of the problem. It could be solved with much less lines of code, but the idea is that the project will grow and grow and we don't want to have mantainabilty problems in the future. We want to reduce the technical debt to the minimum, and here's what we should do to achieve that.

Now, if we want to change the way that we add numbers to a faster one, the only thing we have to do is create a new class, implementing AdderInterface, maybe named UltraFastAdder, with the method 'add' implemented. And the rest of the code will be prepared for that, because the AdderCommandHandler depends on an interface (Dependency Inversion Principle). 

Thanks for reading.