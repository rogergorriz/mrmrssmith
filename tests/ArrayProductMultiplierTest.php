<?php

namespace App\Service;

use PHPUnit\Framework\TestCase;

class ArrayProductMultiplierTest extends TestCase
{
    /** @var  ArrayProductMultiplier */
    private $multiplier;

    public function setUp()
    {
        $this->multiplier = new ArrayProductMultiplier();
    }

    public function testItWorksWithNaturalNumbers()
    {
        $input = [1,2,3,4];
        $this->assertEquals($this->multiplier->multiply($input), 24);
    }

    public function testItWorksWithNegativeNumbers()
    {
        $input = [1,2,-3,4];
        $this->assertEquals($this->multiplier->multiply($input), -24);
    }

    public function testItWorksWithZeros()
    {
        $input = [1,2,-3,4,0];
        $this->assertEquals($this->multiplier->multiply($input), 0);
    }

    public function testItWorksWithFloats()
    {
        $input = [1,2,3,4,0.5];
        $this->assertEquals($this->multiplier->multiply($input), 12);
    }

    public function testItWorksWithSingleNumber()
    {
        $input = [9];
        $this->assertEquals($this->multiplier->multiply($input), 9);
    }
}