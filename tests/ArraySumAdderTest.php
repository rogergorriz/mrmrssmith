<?php

namespace App\Service;

use PHPUnit\Framework\TestCase;

class ArraySumAdderTest extends TestCase
{
    /** @var  ArraySumAdder */
    private $adder;

    public function setUp()
    {
        $this->adder = new ArraySumAdder();
    }

    public function testItWorksWithNaturalNumbers()
    {
        $input = [1,2,3,4];
        $this->assertEquals($this->adder->add($input), 10);
    }

    public function testItWorksWithNegativeNumbers()
    {
        $input = [1,2,-3,4];
        $this->assertEquals($this->adder->add($input), 4);
    }

    public function testItWorksWithZeros()
    {
        $input = [1,2,-3,4,0];
        $this->assertEquals($this->adder->add($input), 4);
    }

    public function testItWorksWithFloats()
    {
        $input = [1,2,3,4,0.5];
        $this->assertEquals($this->adder->add($input), 10.5);
    }

    public function testItWorksWithSingleNumber()
    {
        $input = [9];
        $this->assertEquals($this->adder->add($input), 9);
    }
}