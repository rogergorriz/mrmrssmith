<?php

namespace App\Command;

use App\Service\MultiplierInterface;

class MultiplyNumbersCommandHandler
{

    /**
     * @var MultiplierInterface
     */
    private $multiplier;

    public function __construct(MultiplierInterface $multiplier)
    {
        $this->multiplier = $multiplier;
    }

    public function handle(MultiplyNumbersCommand $command)
    {
        return $this->multiplier->multiply($command->getNumbers());
    }
}