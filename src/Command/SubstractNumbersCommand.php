<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


class SubstractNumbersCommand
{
    private $minuend;
    private $subtrahend;

    public function __construct($minuend, $subtrahend)
    {
        $this->minuend = $minuend;
        $this->subtrahend = $subtrahend;
    }

    /**
     * @return mixed
     */
    public function getSubtrahend()
    {
        return $this->subtrahend;
    }

    /**
     * @return mixed
     */
    public function getMinuend()
    {
        return $this->minuend;
    }
}