<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


class MultiplyNumbersCommand
{
    /**
     * @var array
     */
    private $numbers;

    public function __construct($numbers)
    {
        $this->numbers = $numbers;
    }

    /**
     * @return array
     */
    public function getNumbers()
    {
        return $this->numbers;
    }
}