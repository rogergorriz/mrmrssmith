<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


class DivideNumbersCommand
{
    private $dividend;
    private $divisor;

    public function __construct($dividend, $divisor)
    {
        $this->dividend = $dividend;
        $this->divisor = $divisor;
    }

    /**
     * @return mixed
     */
    public function getDividend()
    {
        return $this->dividend;
    }

    /**
     * @return mixed
     */
    public function getDivisor()
    {
        return $this->divisor;
    }
}