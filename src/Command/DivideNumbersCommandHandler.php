<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


use App\Service\DividerInterface;
use App\Service\SubstracterInterface;

class DivideNumbersCommandHandler
{
    /**
     * @var SubstracterInterface
     */
    private $divider;

    public function __construct(DividerInterface $divider)
    {
        $this->divider = $divider;
    }

    public function handle(DivideNumbersCommand $command)
    {
        return $this->divider->divide($command->getDividend(), $command->getDivisor());
    }
}