<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


use App\Service\AdderInterface;

class AddNumbersCommandHandler
{
    /**
     * @var AdderInterface
     */
    private $adder;

    public function __construct(AdderInterface $adder)
    {
        $this->adder = $adder;
    }

    public function handle(AddNumbersCommand $command)
    {
        return $this->adder->add($command->getNumbers());
    }
}