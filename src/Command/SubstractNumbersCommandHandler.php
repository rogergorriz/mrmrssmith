<?php
/**
 * Created by PhpStorm.
 * User: roger.gorriz
 * Date: 4/4/18
 * Time: 22:45
 */

namespace App\Command;


use App\Service\SubstracterInterface;

class SubstractNumbersCommandHandler
{
    /**
     * @var SubstracterInterface
     */
    private $substracter;

    public function __construct(SubstracterInterface $substracter)
    {
        $this->substracter = $substracter;
    }

    public function handle(SubstractNumbersCommand $command)
    {
        return $this->substracter->substract($command->getMinuend(), $command->getSubtrahend());
    }
}