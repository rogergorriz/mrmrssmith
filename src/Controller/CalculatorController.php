<?php

namespace App\Controller;

use App\Command\AddNumbersCommand;
use App\Command\AddNumbersCommandHandler;
use App\Command\DivideNumbersCommand;
use App\Command\DivideNumbersCommandHandler;
use App\Command\MultiplyNumbersCommand;
use App\Command\MultiplyNumbersCommandHandler;
use App\Command\SubstractNumbersCommand;
use App\Command\SubstractNumbersCommandHandler;
use App\Service\ArrayProductMultiplier;
use App\Service\ArraySumAdder;
use App\Service\SimpleDivider;
use App\Service\SimpleSubstracter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends Controller
{
    /** @Route("/calculator", name="index") */
    public function indexAction()
    {
        return $this->render('calculator.html.twig');
    }


    /** @Route("/addition", name="addition") */
    public function addAction(Request $request)
    {
        $numbers = explode(',',$request->get('numbers'));

        $command = new AddNumbersCommand($numbers);
        $commandHandler = new AddNumbersCommandHandler(new ArraySumAdder());

        $result = $commandHandler->handle($command);

        return $this->json($result);
    }

    /** @Route("/substraction", name="substraction") */
    public function substractAction(Request $request)
    {
        $minuend = $request->get('minuend');
        $subtrahend = $request->get('subtrahend');

        if(!is_numeric($minuend) || !is_numeric($subtrahend))
        {
            return new Response('Values must be numeric', 400);
        }

        $command = new SubstractNumbersCommand($minuend, $subtrahend);
        $commandHandler = new SubstractNumbersCommandHandler(new SimpleSubstracter());
        $result = $commandHandler->handle($command);

        return $this->json($result);
    }

    /** @Route("/multiplication", name="multiplication") */
    public function multiplicationAction(Request $request)
    {
        $numbers = explode(',',$request->get('numbers'));

        $command = new MultiplyNumbersCommand($numbers);
        $commandHandler = new MultiplyNumbersCommandHandler(new ArrayProductMultiplier());

        $result = $commandHandler->handle($command);

        return $this->json($result);
    }

    /** @Route("/division", name="division") */
    public function divisionAction(Request $request)
    {
        $dividend = $request->get('dividend');
        $divisor = $request->get('divisor');

        if(!is_numeric($dividend) || !is_numeric($divisor))
        {
            return new Response('Values must be numeric', 400);
        }

        $command = new DivideNumbersCommand($dividend, $divisor);
        $commandHandler = new DivideNumbersCommandHandler(new SimpleDivider());

        $result = $commandHandler->handle($command);

        return $this->json($result);
    }
}