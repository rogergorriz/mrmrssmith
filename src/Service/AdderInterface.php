<?php

namespace App\Service;

interface AdderInterface
{
    public function add(array $numbers);
}