<?php

namespace App\Service;

class ArrayProductMultiplier implements MultiplierInterface
{
    public function multiply(array $numbers)
    {
        return array_product($numbers);
    }
}