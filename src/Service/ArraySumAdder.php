<?php

namespace App\Service;

class ArraySumAdder implements AdderInterface
{
    public function add(array $numbers)
    {
        return array_sum($numbers);
    }
}