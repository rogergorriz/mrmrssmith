<?php

namespace App\Service;

interface SubstracterInterface
{
    public function substract($minuend, $subtrahend);
}