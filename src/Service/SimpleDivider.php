<?php

namespace App\Service;

class SimpleDivider implements DividerInterface
{

    public function divide($dividend, $divisor)
    {
        return $dividend / $divisor;
    }
}