<?php

namespace App\Service;

class SimpleSubstracter implements SubstracterInterface
{
    public function substract($minuend, $subtrahend)
    {
        return $minuend - $subtrahend;
    }
}