<?php

namespace App\Service;

interface DividerInterface
{
    public function divide($dividend, $divisor);
}