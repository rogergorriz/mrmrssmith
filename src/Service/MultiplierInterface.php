<?php

namespace App\Service;

interface MultiplierInterface
{
    public function multiply(array $numbers);
}